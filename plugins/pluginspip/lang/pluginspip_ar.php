<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/pluginspip?lang_cible=ar
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_afficher_description' => 'عرض الوصف',
	'bouton_effacer' => 'إزالة',
	'bouton_masquer_description' => 'حجب الوصف',
	'bouton_xml' => 'ملف XML',
	'bulle_filtrer_par_categorie' => 'ترشيح الفئة',
	'bulle_rechercher_plugin' => 'بدء البحث',

	// C
	'categorie_toute' => 'كل الفئات',
	'compat_spip' => 'لـSPIP',

	// D
	'derniere_maj' => 'تم التحديث في',

	// E
	'explication_signalement_contact' => 'اذا وجدتم اي خطأ في عرض اي ملحق او في الموقع نفسه يمكنكم توجيه رسالة للمدراء. وحسب نوع الخطأ، لا تنسوا تحديد صفحة الملحق المخطئ.',

	// I
	'info_actualisation_depot_cron' => 'يتم تحديث ملحقات المخازن كل @periode@ ساعة.',
	'info_aucun_depot_disponible' => 'لا توجد مخازن متوافرة.',
	'info_aucun_plugin_disponible' => 'لا توجد ملحقات متوافرة.',
	'info_aucun_plugin_disponible_version' => 'لا يوجد اي ملحق متوافق مع SPIP @version@، يمكن <a href="@url@">توسيع البحث ليشمل كل إصدارات SPIP</a>.',
	'info_aucun_prefixe_disponible' => 'لا توجد بادءات متوافرة.',
	'info_aucune_compatibilite_spip' => 'غير معروفة',
	'info_compatible' => 'متوافق مع:',
	'info_contenu_paquet' => 'يرجى نسخ محتوى ملف paquet.xml بالكامل ولصقه في الحقل أدناه ثم إطلاق الفحص.',
	'info_non_compatible' => 'غير متوافق مع:',
	'info_non_dispo' => 'معلومات غير متوافرة',
	'info_page_non_autorisee' => 'غير مسموح مشاهدة هذه الصفحة',
	'info_plugins_sans_doc' => 'متوافق مع SPIP @branches@ (@nb@)',
	'info_rechercher_plugin' => 'البحث عن ملحق:',
	'info_valider_paquet' => 'تتيح هذه الصفحة فحص ملف<code>paquet.xml</code> لوصف الملحق. اذا لم يتم العثور على اي خطأ، يكون ملفكم <code>paquet.xml</code> سليماً ويمكن استخدامه في ملحقكم. في حال وجود أخطاء يرجى اتباع الخطوات ألموضحة لتصحيحها.',
	'intertitre_contenu_paquet' => 'محتوى ملفكم paquet.xml',
	'intertitre_paquets_contribution' => 'المساهمات الاخرى',
	'intertitre_paquets_plugin' => 'الملحقات',
	'intertitre_resultat_paquet' => 'نتيجة الفحص:',
	'intertitre_stats_generales' => 'ملحقات حسب الفئة',
	'intertitre_stats_plugins_nodoc' => 'ملحقات غير موثقة', # MODIF
	'intertitre_stats_plugins_noupd' => 'ملاحق ضائعة',

	// L
	'label_archive' => 'الأرشيف',
	'label_auteur' => 'المؤلف',
	'label_copyright' => 'الحقوق',
	'label_credit' => 'بفضل',
	'label_etat' => 'الوضعية',
	'label_gestionnaire' => 'الادارة',
	'label_hebergement' => 'موجود في',
	'label_langue_reference' => 'اللغة المرجعية',
	'label_licence' => 'الرخصة',
	'label_maj' => 'أنتج في',
	'label_module' => 'وحدة اللغة',
	'label_nbr_sites' => 'تستخدمه',
	'label_necessite_librairies' => 'يحتاج الى المكتبات',
	'label_necessite_plugins' => 'يحتاج الى الملحقات',
	'label_nom' => 'الاسم',
	'label_taille' => 'الحجم',
	'label_traductions' => 'الترجمات',
	'label_tri' => 'فرز : ',
	'label_tri_maj' => 'حسب تاريخ التحديث',
	'label_tri_nbr' => 'حسب عدد التثبيتات',
	'label_tri_nom' => 'حسب الاسم',
	'label_tri_points' => 'حسب الملاءمة',
	'label_utilise_plugins' => 'متوافق مع',
	'lien_autodoc' => 'التوثيق الآلي',
	'lien_demo' => 'عرض',
	'lien_dev' => 'التطوير',
	'lien_documentation' => 'التوثيق',
	'lien_sources' => 'الرموز البرمجية',

	// P
	'plugin_commits' => 'أحدث التعديلات',
	'plugin_forums' => 'رسائل المنتدى',

	// T
	'titre_bloc_pied_actualite' => 'جديد الملحقات',
	'titre_bloc_pied_utilisation' => 'هذا الموقع المعتمد على SPIP @version@ يستخدم الملحقات',
	'titre_editer_selection' => 'تحرير التحديد',
	'titre_maj_plugins' => 'آخر التحديثات',
	'titre_page_aide' => 'المساعدة',
	'titre_page_apropos' => 'جول ملحقات SPIP',
	'titre_page_depots' => 'المخازن',
	'titre_page_faq' => 'سؤال وجواب',
	'titre_page_plugins' => 'الملحقات',
	'titre_page_prefixes' => 'البادءات',
	'titre_page_signalements' => 'التبليغ عن خطأ',
	'titre_page_statistiques' => 'الاحصاءات',
	'titre_page_telechargements' => 'التحميلات',
	'titre_page_valider_paquet' => 'التصديق على ملف paquet.xml',
	'titre_rss_plugins' => 'تدفق الملحقات',
	'titre_selection' => 'في الواجهة',
	'titre_top_plugins' => 'الـ @nb@ الاكثر استخداماً مع @branche@',
	'toutes_versions_spip' => 'كل الاصدارات'
);
