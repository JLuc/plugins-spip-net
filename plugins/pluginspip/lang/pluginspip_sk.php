<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/pluginspip?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_afficher_description' => 'Zobraziť opis',
	'bouton_effacer' => 'Vymazať',
	'bouton_masquer_description' => 'Skryť opis',
	'bouton_xml' => 'Súbor XML',
	'bulle_filtrer_par_categorie' => 'Zoradiť podľa kategórie',
	'bulle_rechercher_plugin' => 'Spustiť vyhľadávanie',

	// C
	'categorie_toute' => 'Všetky kategórie',
	'compat_spip' => 'pre SPIP',

	// D
	'derniere_maj' => 'Aktualizovaný',

	// E
	'explication_signalement_contact' => 'Ak zistíte chybu pri zobrazení zásuvného modulu alebo na stránke samotnej, máte možnosť nechať odkaz administrátorom.  Dobre si to premyslite, v závislosti od povahy problému uveďte stránku a zásuvný modul, v ktorých je chyba.',

	// I
	'info_actualisation_depot_cron' => 'Zásuvné moduly depozitárov sa automaticky aktualizujú každých @periode@ hodín.',
	'info_aucun_depot_disponible' => 'Žiaden depozitár nie je dostupný.',
	'info_aucun_plugin_disponible' => 'Žiaden zásuvný modul nie je dostupný.',
	'info_aucun_plugin_disponible_version' => 'Pre SPIP @version@ nie je dostupný žiaden zásuvný modul, môžete <a href="@url@">rozšíriť vyhľadávanie na všetky verzie SPIPu.</a>',
	'info_aucun_prefixe_disponible' => 'Žiadne predpony nie sú dostupné.',
	'info_aucune_compatibilite_spip' => 'neposkytuje sa',
	'info_compatible' => 'Kompatibilita: ',
	'info_contenu_paquet' => 'Skopírujte a vložte celý obsah súboru paquet.xml do textového poľa a spustite kontrolu.',
	'info_non_dispo' => 'Údaje nie sú dostupné',
	'info_page_non_autorisee' => 'Na zobrazenie tejto stránky nemáte dostatočné práva',
	'info_rechercher_plugin' => 'Vyhľadať zásuvný modul:',
	'info_valider_paquet' => 'Táto stránka vám umožňuje vykonať formálnu kontrolu súboru <code>paquet.xml</code> s opisom zásuvného modulu. Ak sa nenájdu žiadne chyby, potom je váš súbor <code>paquet.xml</code> schválený a môžete ho bez problémov používať v zásuvnom module. V opačnom prípade postupujte podľa pokynov, aby sa chyby odstránili.',
	'intertitre_contenu_paquet' => 'Obsah vášho súboru paquet.xml',
	'intertitre_paquets_contribution' => 'Ostatné príspevky',
	'intertitre_paquets_plugin' => 'Zásuvné moduly',
	'intertitre_resultat_paquet' => 'Výsledok kontroly:',

	// L
	'label_archive' => 'Archív',
	'label_auteur' => 'Autor',
	'label_copyright' => 'Autorské práva',
	'label_credit' => 'Spolupracovali',
	'label_etat' => 'Stav',
	'label_gestionnaire' => 'Manažér',
	'label_hebergement' => 'Nachádza sa na serveri',
	'label_langue_reference' => 'Referenčný jazyk',
	'label_licence' => 'Licencia',
	'label_maj' => 'Vytvorený',
	'label_module' => 'Jazykový modul',
	'label_nbr_sites' => 'Využíva',
	'label_necessite_librairies' => 'Vyžaduje si knižnice',
	'label_necessite_plugins' => 'Vyžaduje si zásuvné moduly',
	'label_nom' => 'Meno',
	'label_taille' => 'Veľkosť',
	'label_traductions' => 'Preklady',
	'label_tri' => 'Triedenie: ',
	'label_tri_maj' => 'podľa dátumu aktualizácie',
	'label_tri_nbr' => 'podľa počtu inštalácií',
	'label_tri_nom' => 'podľa mena',
	'label_tri_points' => 'podľa relevantnosti',
	'label_utilise_plugins' => 'Kompatibilné s',
	'lien_demo' => 'Demo',
	'lien_dev' => 'Vývoj',
	'lien_documentation' => 'Dokumentácia',
	'lien_sources' => 'Zdrojový kód',

	// P
	'plugin_commits' => 'Posledné zmeny',
	'plugin_forums' => 'Príspevky v diskusnom fóre',

	// T
	'titre_bloc_pied_actualite' => 'Novinky o zásuvných moduloch',
	'titre_bloc_pied_utilisation' => 'Táto stránka v SPIPe @version@ využíva zásuvné moduly',
	'titre_editer_selection' => 'Upraviť výber',
	'titre_maj_plugins' => 'Naposledy aktualizované',
	'titre_page_aide' => 'Pomocník',
	'titre_page_apropos' => 'O Zásuvných moduloch SPIPu',
	'titre_page_depots' => 'Depozitáre',
	'titre_page_faq' => 'Časté otázky',
	'titre_page_plugins' => 'Zásuvné moduly',
	'titre_page_prefixes' => 'Predpony',
	'titre_page_signalements' => 'Nahlásiť chybu',
	'titre_page_telechargements' => 'Na stiahnutie',
	'titre_page_valider_paquet' => 'Skontrolovať paquet.xml',
	'titre_rss_plugins' => 'Kanály zásuvných modulov',
	'titre_selection' => 'V centre pozornosti',
	'titre_top_plugins' => '@nb@ najpoužívanejších', # MODIF
	'toutes_versions_spip' => 'Všetky verzie'
);
