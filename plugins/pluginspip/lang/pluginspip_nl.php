<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/pluginspip?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_afficher_description' => 'Toon de omschrijving',
	'bouton_effacer' => 'Wissen',
	'bouton_masquer_description' => 'Verberg de omschrijving',
	'bouton_xml' => 'XML bestand',
	'bulle_filtrer_par_categorie' => 'Filter per categorie',
	'bulle_rechercher_plugin' => 'Zoeken',

	// C
	'categorie_toute' => 'Alle categorieën',
	'compat_spip' => 'voor SPIP',

	// D
	'derniere_maj' => 'Aangepast op',

	// E
	'explication_signalement_contact' => 'Laat een bericht achter voor de beheerder wanneer je een fout vindt op één van de webpagina’s. Geef (afhankelijk van het probleem) de pagina en de falende plugin aan.',

	// I
	'info_actualisation_depot_cron' => 'De plugins worden om de @periode@ uur automatisch geupdate.',
	'info_aucun_depot_disponible' => 'Geen depot beschikbaar.',
	'info_aucun_plugin_disponible' => 'Geen plugin beschikbaar.',
	'info_aucun_plugin_disponible_version' => 'Geen plugin beschikbaar voor SPIP @version@. Je kunt de zoekopdracht uitbreiden naar <a href="@url@">alle versies van SPIP</a>.',
	'info_aucun_prefixe_disponible' => 'Geen prefix beschikbaar.',
	'info_aucune_compatibilite_spip' => 'niet vermeld',
	'info_compatible' => 'Compatibel: ',
	'info_contenu_paquet' => 'Kopieer & plak de exacte inhoud van paquet.xml in het onderstaande tekstveld en start de validatie.',
	'info_non_compatible' => 'Niet-compatibel: ',
	'info_non_dispo' => 'Informatie niet beschikbaar',
	'info_page_non_autorisee' => 'Je mag deze bladzijde niet raadplegen',
	'info_plugins_sans_doc' => 'Compatibel met SPIP @branches@ (@nb@)',
	'info_rechercher_plugin' => 'Een plugin zoeken:',
	'info_valider_paquet' => 'Met deze pagina kun je een <code>paquet.xml</code> valideren die een plugin beschrijft. Worden geen fouten gevonden, dan is de <code>paquet.xml</code> geldig en kan deze zonder probleem in je plugin gebruikt worden. Volg in het andere geval de instructies om de fout(en) te corrigeren.',
	'intertitre_contenu_paquet' => 'inhoud paquet.xml',
	'intertitre_paquets_contribution' => 'Andere bijdragen',
	'intertitre_paquets_plugin' => 'Plugins',
	'intertitre_resultat_paquet' => 'Resultaat validatie:',
	'intertitre_stats_generales' => 'Plugins per categorie',
	'intertitre_stats_plugins_nodoc' => 'Plugins zonder documentatie', # MODIF
	'intertitre_stats_plugins_noupd' => 'Verdwenen plugins',

	// L
	'label_archive' => 'Pakket',
	'label_auteur' => 'Auteur',
	'label_copyright' => 'Copyright',
	'label_credit' => 'Credits',
	'label_etat' => 'Status',
	'label_gestionnaire' => 'Beheerd door',
	'label_hebergement' => 'Gehost door',
	'label_langue_reference' => 'Referentietaal',
	'label_licence' => 'Licentie',
	'label_maj' => 'Gebouwd op',
	'label_module' => 'Taalmodule',
	'label_nbr_sites' => 'Gebruikt door',
	'label_necessite_librairies' => 'Vereiste bibliotheken',
	'label_necessite_plugins' => 'Vereiste plugins',
	'label_nom' => 'Naam',
	'label_taille' => 'Grootte',
	'label_traductions' => 'Vertalingen',
	'label_tri' => 'Sortering: ',
	'label_tri_maj' => 'op aanpassingsdatum',
	'label_tri_nbr' => 'op aantal installaties',
	'label_tri_nom' => 'op naam',
	'label_tri_points' => 'op relevantie',
	'label_utilise_plugins' => 'Compatibel met',
	'lien_autodoc' => 'Autodoc',
	'lien_demo' => 'Demonstratie',
	'lien_dev' => 'Ontwikkeling',
	'lien_documentation' => 'Documentatie',
	'lien_sources' => 'Broncode',

	// P
	'plugin_commits' => 'Recente aanpassingen',
	'plugin_forums' => 'Forumberichten',

	// T
	'titre_bloc_pied_actualite' => 'Actualiteit rond Plugins',
	'titre_bloc_pied_utilisation' => 'Deze site wordt aangedreven door SPIP @version@ met de volgende plugins',
	'titre_editer_selection' => 'Selectie aanpassen',
	'titre_maj_plugins' => 'Recente aanpassingen',
	'titre_page_aide' => 'Hulp',
	'titre_page_apropos' => 'Over Plugins SPIP',
	'titre_page_depots' => 'Depots',
	'titre_page_faq' => 'FAQ',
	'titre_page_plugins' => 'Plugins',
	'titre_page_prefixes' => 'Prefixes',
	'titre_page_signalements' => 'Een fout rapporteren',
	'titre_page_statistiques' => 'Statistieken',
	'titre_page_telechargements' => 'Downloads',
	'titre_page_valider_paquet' => 'Een paquet.xml valideren',
	'titre_rss_plugins' => 'Plugins RSS feed',
	'titre_selection' => 'Geselecteerd',
	'titre_top_plugins' => 'De @nb@ meest populaire in @branche@',
	'toutes_versions_spip' => 'Alle versies'
);
