<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-pluginspip?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'pluginspip_description' => 'Tento zásuvný modul je verzia šablóny podstránky Plugins SPIP pre rok 2011, ktorá patrí  do galaxie SPIPu.
  _ Najmä vďaka zásuvnému modulu SVP umožňuje vypísať všetky údaje na stránkach zásuvných modulov, ktoré sa upravujú a aktualizujú automaticky.',
	'pluginspip_slogan' => 'Šablóna Z stránky Zásuvné moduly SPIPu prevádzkovaných so SVP'
);
