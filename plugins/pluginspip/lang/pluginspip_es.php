<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/pluginspip?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_afficher_description' => 'Mostrar la descripción',
	'bouton_effacer' => 'Borrar',
	'bouton_masquer_description' => 'Esconder la descripción',
	'bouton_xml' => 'Archivo XML',
	'bulle_filtrer_par_categorie' => 'Filtrar por categoría',
	'bulle_rechercher_plugin' => 'Poner en marcha la búsqueda',

	// C
	'categorie_toute' => 'Todas las categorías',
	'compat_spip' => 'para SPIP',

	// D
	'derniere_maj' => 'Actualizado el',

	// E
	'explication_signalement_contact' => 'Si detecta un error en la visualización de un plugin o en el propio sitio, tiene la posibilidad de dejar un mensaje a los administradores. Procure, siguiendo la naturaleza del problema, precisar la página y el plugin defectuoso. ',

	// I
	'info_actualisation_depot_cron' => 'Los plugins de los depósitos se actualizan automáticamente todas las @periode@ hora(s).',
	'info_aucun_depot_disponible' => 'Ningún repositorio disponible.',
	'info_aucun_plugin_disponible' => 'Ningún plugin disponible.',
	'info_aucun_plugin_disponible_version' => 'Ningún plugin disponible para SPIP @version@, puede <a href="@url@">extender la búsqueda a todas las versiones de SPIP</a>.',
	'info_aucun_prefixe_disponible' => 'Ningún prefijo disponible.',
	'info_aucune_compatibilite_spip' => 'no comunicada',
	'info_compatible' => 'Compatible: ',
	'info_contenu_paquet' => 'Copiar-pegar el contenido exacto de su paquet.xml en la siguiente zona de entrada y poner en marcha la validación.',
	'info_non_dispo' => 'Información no disponible',
	'info_page_non_autorisee' => 'No está autorizado para consultar esta página',
	'info_rechercher_plugin' => 'Buscar un plugin:',
	'info_valider_paquet' => 'Esta página le permite validar formalmente un archivo <code>paquet.xml</code> de descripción de un plugin. Si no se detecta error alguno, entonces su <code>paquet.xml</code> es válido y puede ser utilizado sin problema en su plugin. En caso contrario, siga las indicaciones para corregir los errores.',
	'intertitre_contenu_paquet' => 'Contenido de su paquet.xml',
	'intertitre_paquets_contribution' => 'Otras contribuciones',
	'intertitre_paquets_plugin' => 'Plugins',
	'intertitre_resultat_paquet' => 'Resultado de la validación:',

	// L
	'label_archive' => 'Archivo',
	'label_auteur' => 'Autor',
	'label_copyright' => 'Copyright',
	'label_credit' => 'Créditos',
	'label_etat' => 'Estado',
	'label_gestionnaire' => 'Gestor',
	'label_hebergement' => 'Hospedado por',
	'label_langue_reference' => 'Idioma de referencia',
	'label_licence' => 'Licencia',
	'label_maj' => 'Generado el',
	'label_module' => 'Módulo de idioma',
	'label_nbr_sites' => 'Utilizado por',
	'label_necessite_librairies' => 'Necesita bibliotecas',
	'label_necessite_plugins' => 'Necesita plugins',
	'label_nom' => 'Nombre',
	'label_taille' => 'Tamaño',
	'label_traductions' => 'Traducciones',
	'label_tri' => 'Clasificar:',
	'label_tri_maj' => 'por fecha de actualización',
	'label_tri_nbr' => 'por número de instalaciones',
	'label_tri_nom' => 'por nombre',
	'label_tri_points' => 'por conveniencia',
	'label_utilise_plugins' => 'Compatible con',
	'lien_demo' => 'Demostración',
	'lien_dev' => 'Desarrollo',
	'lien_documentation' => 'Documentación',
	'lien_sources' => 'Código fuente',

	// P
	'plugin_commits' => 'Últimas modificaciones',
	'plugin_forums' => 'Mensajes de foro',

	// T
	'titre_bloc_pied_actualite' => 'Actualidad de los plugins',
	'titre_bloc_pied_utilisation' => 'Este sitio bajo SPIP @version@ utiliza los plugins',
	'titre_editer_selection' => 'Editar la selección',
	'titre_maj_plugins' => 'Actualizaciones recientes',
	'titre_page_aide' => 'Ayuda',
	'titre_page_apropos' => 'Acerca de Plugins SPIP',
	'titre_page_depots' => 'Depósitos',
	'titre_page_faq' => 'FAQ',
	'titre_page_plugins' => 'Plugins',
	'titre_page_prefixes' => 'Prefijos',
	'titre_page_signalements' => 'Reportar un error',
	'titre_page_telechargements' => 'Descargas',
	'titre_page_valider_paquet' => 'Validar un paquet.xml',
	'titre_rss_plugins' => 'Fuente de plugins',
	'titre_selection' => 'Destacado',
	'titre_top_plugins' => 'Los @nb@ más utilizados', # MODIF
	'toutes_versions_spip' => 'Todas las versiones'
);
