<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-galaxie/plugins-spip-net.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'pluginspip_description' => 'Ce plugin est la version 2011 du squelette du site Plugins SPIP appartenant à la galaxie SPIP. 
_ Il permet, en utilisant principalement le plugin SVP, de restituer toutes les informations des plugins SPIP dans des pages adaptées et mises à jour automatiquement.',
	'pluginspip_slogan' => 'Squelette Z du site Plugins SPIP motorisé par SVP'
);
