<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/pluginspip?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_afficher_description' => 'Show the description',
	'bouton_effacer' => 'Erase',
	'bouton_masquer_description' => 'Hide the description',
	'bouton_xml' => 'XML file',
	'bulle_filtrer_par_categorie' => 'Filter by category',
	'bulle_rechercher_plugin' => 'Search',

	// C
	'categorie_aucune' => 'No category',
	'categorie_auteur' => 'Authentication, author, authorization',
	'categorie_communication' => 'Communication, interactivity, messaging ',
	'categorie_date' => 'Agendas, calendar, date',
	'categorie_divers' => 'New objects, external services',
	'categorie_edition' => 'Editing, printing, writing',
	'categorie_maintenance' => 'Configuration, maintenance',
	'categorie_multimedia' => 'Images, galeries, multimedia',
	'categorie_navigation' => 'Navigation, search organization',
	'categorie_outil' => 'Development tool',
	'categorie_performance' => 'Optimization, performance, security',
	'categorie_squelette' => 'Template',
	'categorie_statistique' => 'SEO, statistics',
	'categorie_theme' => 'Theme',
	'categorie_toute' => 'All categories',
	'compat_spip' => 'for SPIP',
	'config_utiliser_categorie' => 'Use the plugins’ category in the plugins display or search (not necessary in runtime mode)',
	'config_utiliser_tag' => 'Use plugin tags in plugin views and searches (not required in runtime mode) ',

	// D
	'derniere_maj' => 'Updated on',

	// E
	'etat_deprecie' => 'deprecated',
	'explication_signalement_contact' => 'Please, leave a message to the administrators when detecting any errors within the website pages. Depending on the issue, specify the page and the faulty plugin.',

	// I
	'info_actualisation_depot_cron' => 'The plugins are automatically updated every  @periode@ hour(s).',
	'info_aucun_depot_disponible' => 'No repository available.',
	'info_aucun_plugin_disponible' => 'No plugin available.',
	'info_aucun_plugin_disponible_version' => 'No plugin available for SPIP @version@, you can <a href="@url@">extend the search to all versions of SPIP</a>.',
	'info_aucun_prefixe_disponible' => 'No prefix available.',
	'info_aucune_compatibilite_spip' => 'not provided',
	'info_compatible' => 'Compatible: ',
	'info_contenu_paquet' => 'Copy & paste the exact content of your paquet.xml in the textarea below and start the validation.',
	'info_non_compatible' => 'Not compatible: ',
	'info_non_dispo' => 'Information not available',
	'info_page_non_autorisee' => 'You are not allowed to consult this page',
	'info_plugins_sans_doc' => 'Compatible with SPIP @branches@ (@nb@)',
	'info_rechercher_plugin' => 'Search a plugin :',
	'info_valider_paquet' => 'This page allows you to validate a <code> paquet.xml </code> describing a plugin. If no errors are detected then your <code> paquet.xml </code> is valid and can be used without problem in your plugin. Otherwise, follow the instructions to correct the errors.',
	'intertitre_contenu_paquet' => 'paquet.xml content',
	'intertitre_paquets_contribution' => 'Others contributions',
	'intertitre_paquets_plugin' => 'Plugins',
	'intertitre_resultat_paquet' => 'Validation output:',
	'intertitre_stats_generales' => 'Plugins by category',
	'intertitre_stats_plugins_nodoc' => 'Plugins without documentation', # MODIF
	'intertitre_stats_plugins_noupd' => 'Plugins lost en route',

	// L
	'label_archive' => 'Package',
	'label_auteur' => 'Author',
	'label_categorie' => 'Category',
	'label_copyright' => 'Copyright',
	'label_credit' => 'Credits',
	'label_etat' => 'Status',
	'label_gestionnaire' => 'Managed by',
	'label_hebergement' => 'Hosted by',
	'label_langue_reference' => 'Reference language',
	'label_licence' => 'License',
	'label_maj' => 'Built on',
	'label_module' => 'Language module',
	'label_nbr_sites' => 'Used by',
	'label_necessite_librairies' => 'Required libraries',
	'label_necessite_plugins' => 'Required plugins',
	'label_nom' => 'Name',
	'label_taille' => 'Size',
	'label_traductions' => 'Translations',
	'label_tri' => 'Sort: ',
	'label_tri_maj' => 'by update date',
	'label_tri_nbr' => 'by installations',
	'label_tri_nom' => 'by name',
	'label_tri_points' => 'by relevance',
	'label_utilise_plugins' => 'Compatible with',
	'lien_autodoc' => 'Autodoc',
	'lien_demo' => 'Demonstration',
	'lien_dev' => 'Development',
	'lien_documentation' => 'Documentation',
	'lien_sources' => 'Source code',

	// P
	'plugin_commits' => 'Recent changes',
	'plugin_forums' => 'Forum messages',

	// T
	'titre_bloc_pied_actualite' => 'Plugins breaking news',
	'titre_bloc_pied_utilisation' => 'This site is powered by SPIP @version@ and the following plugins',
	'titre_editer_selection' => 'Edit selection',
	'titre_maj_plugins' => 'Recent updates',
	'titre_page_aide' => 'Help',
	'titre_page_apropos' => 'About Plugins SPIP',
	'titre_page_depots' => 'Repositories',
	'titre_page_faq' => 'FAQ',
	'titre_page_plugins' => 'Plugins',
	'titre_page_prefixes' => 'Prefixes',
	'titre_page_signalements' => 'Report an error',
	'titre_page_statistiques' => 'Statistics',
	'titre_page_telechargements' => 'Downloads',
	'titre_page_valider_paquet' => 'Validate a paquet.xml',
	'titre_rss_plugins' => 'Plugins RSS feed',
	'titre_selection' => 'Featured',
	'titre_top_plugins' => '@nb@ most used in @branche@',
	'toutes_versions_spip' => 'All versions'
);
