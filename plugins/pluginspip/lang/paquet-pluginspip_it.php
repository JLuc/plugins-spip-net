<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-pluginspip?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'pluginspip_description' => 'Questo plugin è la versione 2011 del template per il sito SPIP Plugins appartenente alla galassia SPIP.
Consente, principalmente utilizzando il plugin SVP, di ripristinare tutte le informazioni dei plugin SPIP in pagine adattate e aggiornate automaticamente.',
	'pluginspip_slogan' => 'Template Z del sito Plugins SPIP powered by SVP'
);
