<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/pluginspip?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_xml' => 'پرونده‌ي ايكس .ام.ال',
	'bulle_filtrer_par_categorie' => 'مرتب سازي بر اساس مقوله',
	'bulle_rechercher_plugin' => 'آغاز جستجو',

	// C
	'categorie_toute' => 'تمام مقوله‌ها',
	'compat_spip' => 'براي اسپيپ',

	// D
	'derniere_maj' => 'به هنگام سازي‌ي',

	// E
	'explication_signalement_contact' => 'اگر در نمايش يك پلاگين يا خود سايت خطايي ديديد، مي‌توانيد براي آدمين‌ها پيام بگذاريد. با توجه به ماهيت پيام، صفحه‌ و پلاگين معيوب را مشخص سازيد.
',

	// I
	'info_actualisation_depot_cron' => 'پلاگين‌هاي مخزن‌ها به طور خودكار هر @periode@ ساعت به هنگام‌ مي‌شوند.

',
	'info_aucun_depot_disponible' => 'هيچ مخزني در دسترس نيست.',
	'info_aucun_plugin_disponible' => 'هيچ پلاگيني در دسترس نيست.',
	'info_aucun_plugin_disponible_version' => ' براي اسپيپ نسخه‌ي @version@ هيچ پلاگيني در دسترس نيست، مي‌توانيد <a href="@url@">جستجو را به تمام نسخه‌هاي اسپيپ گسترش دهيد. </a>',
	'info_aucun_prefixe_disponible' => 'هيچ پيش‌وندي در دسترس نيست.',
	'info_aucune_compatibilite_spip' => 'فراهم نشده (؟)',
	'info_non_dispo' => 'اطلاعاتي در دسترس نيست',
	'info_rechercher_plugin' => 'جستجوي يك پلاگين', # MODIF
	'intertitre_paquets_contribution' => 'كمك‌هاي ديگران ',
	'intertitre_paquets_plugin' => 'پلاگين‌ها',

	// L
	'label_archive' => 'آرشيو',
	'label_auteur' => 'نويسنده',
	'label_copyright' => 'كپي رايت',
	'label_credit' => 'اعتبارها',
	'label_etat' => 'كشور',
	'label_gestionnaire' => 'با مديريت',
	'label_hebergement' => 'ميزباني‌شده توسط',
	'label_langue_reference' => 'زبان مرجع',
	'label_licence' => 'پروانه',
	'label_maj' => 'ساخته شده بر مبناي',
	'label_module' => 'ماجول زبان',
	'label_nbr_sites' => 'مورد استفاده توسط',
	'label_necessite_librairies' => 'آرشيو‌هاي مورد نياز ',
	'label_necessite_plugins' => 'پلاگين‌هاي مور نياز',
	'label_nom' => 'نام',
	'label_taille' => 'اندازه',
	'label_traductions' => 'ترجمه‌ها',
	'label_tri' => 'تيتر: ',
	'label_tri_maj' => 'بر اساس تاريخ به روز رساني ',
	'label_tri_nbr' => 'براساس تعداد نصب‌ها',
	'label_tri_nom' => 'براساس نام',
	'label_tri_points' => 'براساس ارتباط ',
	'label_utilise_plugins' => 'منطبق با ',
	'lien_demo' => 'نمايش',
	'lien_dev' => 'توسعه',
	'lien_documentation' => 'سند',
	'lien_sources' => 'كد منبع',

	// P
	'plugin_commits' => 'آخرين اصلاح‌ها',
	'plugin_forums' => 'پيام‌هاي سخنگاه',

	// T
	'titre_bloc_pied_actualite' => 'خبرهاي پلاگين‌ها',
	'titre_bloc_pied_utilisation' => 'اين سايت تحت اسپيپ نسخه‌ي @version@ از پلاگين‌ها استفاده مي‌كند',
	'titre_editer_selection' => 'ويرايش گزينه ', # MODIF
	'titre_maj_plugins' => 'به روز‌رساني‌هاي اخير',
	'titre_page_aide' => 'كمك',
	'titre_page_depots' => 'مخزن‌ها',
	'titre_page_plugins' => 'پلاگنين‌ها',
	'titre_page_prefixes' => 'پيشوندها',
	'titre_page_signalements' => 'گزارش يك خطا',
	'titre_page_telechargements' => 'بارگذاري‌ها',
	'titre_rss_plugins' => 'خوراك پلاگين‌ها',
	'titre_selection' => 'چهره',
	'titre_top_plugins' => '@nb@ استفاه‌ي بيشتر', # MODIF
	'toutes_versions_spip' => 'تمام نسخه‌ها'
);
