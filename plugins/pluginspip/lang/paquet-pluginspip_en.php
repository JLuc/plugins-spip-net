<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-pluginspip?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'pluginspip_description' => 'This plugin is the "2011" version of the skeleton from the site Plugins SPIP belonging to the SPIP galaxy.
_ It allows, using mainly the SVP plugin, to return all information of the SPIP plugins in automatically adapted and updated pages.',
	'pluginspip_slogan' => 'Z skeleton of the site Plugins SPIP powered by SVP'
);
