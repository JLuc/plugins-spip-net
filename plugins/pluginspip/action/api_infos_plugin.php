<?php
/**
 * Infos plugin
 * Licence GPL3
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

function action_api_infos_plugin_dist(){

	// Construction du where : si les arguments sont fournis on peut alors chercher les informations sur le plugin.
	if ($l = _request('lien_doc'))
		$where = "pa.lien_doc LIKE" . sql_quote("$l%");
	if ($p = _request('arg'))
		$where = "pa.prefixe=" . sql_quote($p);

	$retour = array();
	if ($where) {
		// Récupération des urls des dépôts pour construire l'url complète de l'archive.
		$depots = sql_allfetsel(array('id_depot', 'url_archives'), 'spip_depots');
		if ($depots) {
			// On range le tableau des dépots sous la forme [id_depot] = url archives
			$urls_archives = array_column($depots, 'url_archives', 'id_depot');

			// Pouvoir utiliser spip_version_compare().
			include_spip('plugins/installer');

			// Récupération des paquets ayant le bon préfixe et/ou le bon lien de doc.
			$paquets = sql_allfetsel(
				'pa.id_depot,pa.prefixe,pa.version,pa.branches_spip,pa.lien_doc,pa.logo,pa.nom_archive,pa.maj_archive,pa.compatibilite_spip,pa.etat, pl.categorie, pl.nom',
				'spip_paquets as pa JOIN spip_plugins as pl ON pa.id_plugin=pl.id_plugin',
				$where,
				'',
				'pa.version DESC'
			);
			foreach ($paquets as $_paquet) {
				if ($_paquet['nom_archive'] and $_paquet['id_depot']) {
					// On prépare le bloc paquet à enregistrer par la suite pour chaque branche SPIP concernée.
					// -- Supprimer la normalisation de la version (00x.00y.00z) nécessaire au tri.
					// -- TODO : il existe la fonction denormaliser_version() qui conserve les -dev ou autres ??
					$pv = explode('.', $_paquet['version']);
					$pv = array_map('intval', $pv);
					$pv = implode('.', $pv);
					$infos_paquet = array(
						'version' => $pv,
						'lien_doc' => $_paquet['lien_doc'],
						'logo' => $_paquet['logo'],
						'archive' => $urls_archives[$_paquet['id_depot']] . '/' . $_paquet['nom_archive'],
						'date' => $_paquet['maj_archive'],
						'categorie' => $_paquet['categorie'],
						'nom' => $_paquet['nom'],
						'etat' => $_paquet['etat']
					);

					// On boucle sur chaque branche supportée par le paquet et on insère la version la plus récente
					// du plugin dans le tableau de sortie.
					$prefixe = $_paquet['prefixe'];
					$branches = explode(',', $_paquet['branches_spip']);
					foreach ($branches as $_v) {
						$vindex = "SPIP-${_v}";
						if (empty($retour['plugins'][$prefixe][$vindex])
						or spip_version_compare($retour['plugins'][$prefixe][$vindex]['version'], $infos_paquet['version'], '<=')) {
							// Enregistrement du paquet : premier ou plus récent
							$retour['plugins'][$prefixe][$vindex] = $infos_paquet;
						}
					}
				}
			}
		}
	}

	// Traitement du retour
	if (!$retour){
		include_spip('inc/headers');
		http_status(404);
		echo "404 Not Found";
	}
	else {
		$retour = json_encode($retour);
		$content_type = "application/json";
		header("Content-type: $content_type; charset=utf-8");
		echo $retour;
	}
}
