<?php

// Catégories
// ----------
function categorie_lister($information) {
	static $liste = array();

	if (empty($liste[$information])) {
		// On ne traite que les catégories racine
		$filtres = array('profondeur' => 0);
		if ($information === 'id') {
			$categories = type_plugin_repertorier('categorie', $filtres, 'identifiant');
			$liste[$information] = array_column($categories, 'identifiant');
			ksort($liste[$information]);
			$liste[$information][] = 'aucune';
		} elseif ($information === 'traduction') {
			$traductions = type_plugin_repertorier('categorie', $filtres, array('identifiant', 'titre'));
			$liste[$information] = array_column($traductions, 'titre', 'identifiant');
			ksort($liste[$information]);
			$liste[$information]['aucune'] = _T('pluginspip:categorie_aucune');
		} else {
			$ids = type_plugin_repertorier('categorie', $filtres, 'id_mot');
			$from = array('spip_couleur_objet_liens as t1', 'spip_mots as t2');
			$select = array('t2.identifiant as id', 'SUBSTR(t1.couleur_objet, 2, 6) as couleur');
			$where = array(
				't1.objet=' . sql_quote('mot'),
				sql_in('t1.id_objet', array_column($ids, 'id_mot')),
				't1.id_objet=t2.id_mot'
			);
			$couleurs = sql_allfetsel($select, $from, $where);
			$liste[$information] = array_column($couleurs, 'couleur', 'id');
			ksort($liste[$information]);
			$liste[$information]['aucune'] = '0a63b2';
		}
	}

	return $liste[$information];
}

/**
 * Affecte une couleur à un type de catégorie de plugin.
 *
 * A priori ne sert plus depuis que les couleurs sont gérées depuis des variables SCSS
 *
 * @param string $categorie
 *     Type de catégorie (auteur, communication, date...)
 *
 * @return string
 *     Couleur en hexa (sans le dièse initial)
 **/
function categorie_affecter_couleur($categorie) {

	$couleurs = categorie_lister('couleur');
	if (
		!$categorie
		or (!array_key_exists($categorie, $couleurs))
		or ($categorie === 'defaut')
	) {
		$couleur = 'b9274d';
	} else {
		$couleur = $couleurs[$categorie];
	}

	return $couleur;
}

/**
 * Traduit un type de catégorie de plugin.
 *
 * @param string $categorie
 *     Type de catégorie (auteur, communication, date...)
 *
 * @return string
 *     Titre complet et traduit de la catégorie
 **/
function categorie_traduire($categorie) {
	$traduction = '';
	if ($categorie) {
		include_spip('inc/svptype_type_plugin');
		$traduction = ($categorie === 'aucune')
			? _T('pluginspip:categorie_aucune')
			: extraire_multi(type_plugin_lire('categorie', $categorie, 'titre'));
	}

	return $traduction;
}

/**
 * Compile la balise `#CATEGORIES_LISTE`.
 *
 * Cette balise retourne un tableau listant soit les traductions, soit les id ou soit les couleurs de chaque catégorie.
 *
 * Accepte 1 paramètre :
 * 1) le type d'information : `id`, `traduction` ou `couleur`
 *
 * @example
 *     #CATEGORIES_LISTE, correspond à id
 *     #CATEGORIES_LISTE{traduction}
 *     #CATEGORIES_LISTE{couleur}
 *
 * @balise
 * @see categorie_lister()
 * @param Champ $p
 *     Pile au niveau de la balise
 * @return Champ
 *     Pile complétée par le code à générer
 **/
function balise_CATEGORIES_LISTE($p) {
	// information, peut être 'id', 'traduction' ou 'couleur'
	if (!$information = interprete_argument_balise(1, $p)) {
		$information = "'id'";
	}
	$p->code = 'categorie_lister(' . $information . ')';

	return $p;
}

/**
 * Critère de restriction d'une boucle plugins sur sa catégorie.
 * Ce critère ne supporte pas la négation.
 *
 * Fonctionne sur les tables spip_mots et spip_groupes_mots.
 *
 * @package SPIP\SVPTYPE\TYPOLOGIE_PLUGIN\CRITERE
 *
 * @uses calculer_critere_typologie_plugin()
 *
 * @critere
 *
 * @example
 *   {categorie identifiant_categorie}, indique une catégorie précise affectée
 *   {categorie aucune}, indique aucune catégorie affectée
 *   {categorie toute}, indique une quelconque catégorie affectée
 *   {categorie #ENV{categorie}}, #ENV{categorie} désigne forcément un identifiant de categorie
 *   {categorie?}, indique que la catégorie est dans le contexte sinon le critère est ignoré
 *   {categorie #GET{categorie}}, #GET{categorie} désigne forcément un identifiant de categorie
 *
 * @param string  $idb     Identifiant de la boucle.
 * @param array   $boucles AST du squelette.
 * @param Critere $critere Paramètres du critère dans la boucle.
 *
 * @return void
 */
function critere_PLUGINS_categorie_dist($idb, &$boucles, $critere) {

	// Initialisation de la boucle concernée.
	$boucle = &$boucles[$idb];

	// Initialiser le hash de calcul de la condition qui ne sera rempli que si le critère est correct.
	$hash = '';

	// Calcul de la condition
	if (!empty($critere->param)) {
		// On identifie la categorie passée dans le critère.
		if (
			isset($critere->param[0])
			and ($parametre = $critere->param[0])
		) {
			$categorie = calculer_liste($parametre, [], $boucles, $boucle->id_parent);
			$hash = '
			$where = $conditionner(\'categorie\',' . $categorie . ');';
		}
	} elseif (
		!empty($critere->cond)
		and ($critere->cond === '?')
	) {
		// Pas de catégorie explicite dans l'appel du critere mais un point d'interrogation.
		// -> on regarde si la catégorie est dans le contexte
		$hash = '
		$categorie = interdire_scripts(entites_html(table_valeur($Pile[0]??[], (string)\'categorie\', null), true));
		$where = $conditionner(\'categorie\',$categorie);
		';
	}

	if ($hash) {
		$boucle->hash .=  '
			// CATEGORIE PLUGIN
			$conditionner = \'calculer_critere_typologie_plugin\';';
		$boucle->hash .= $hash;
		$boucle->where[] = '$where';
	}
}

/**
 * Traduit un type de catégorie de plugin.
 *
 * @param string $tag
 *     Identifiant de tag (auteur, communication, date...)
 *
 * @return string
 *     Titre complet et traduit du tag
 **/
function tag_traduire($tag) {
	$traduction = '';
	if ($tag) {
		include_spip('inc/svptype_type_plugin');
		$traduction = ($tag === 'aucun')
			? _T('pluginspip:tag_aucun')
			: extraire_multi(type_plugin_lire('tag', $tag, 'titre'));
	}

	return $traduction;
}


/**
 * Critère de restriction d'une boucle plugins sur un tag.
 * Ce critère ne supporte pas la négation.
 *
 * Fonctionne sur les tables spip_mots et spip_groupes_mots.
 *
 * @package SPIP\SVPTYPE\TYPOLOGIE_PLUGIN\CRITERE
 *
 * @uses calculer_critere_typologie_plugin()
 *
 * @critere
 *
 * @example
 *   {tag identifiant_tag}, indique un tag précis affecté
 *   {tag aucun}, indique aucun tag affecté
 *   {tag tout}, indique un quelconque tag
 *   {tag #ENV{tag}}, #ENV{tag} désigne forcément un identifiant de tag
 *   {tag?}, indique que le tag est dans le contexte sinon le critère est ignoré
 *   {tag #GET{tag}}, #GET{tag} désigne forcément un identifiant de tag
 *
 * @param string  $idb     Identifiant de la boucle.
 * @param array   $boucles AST du squelette.
 * @param Critere $critere Paramètres du critère dans la boucle.
 *
 * @return void
 */
function critere_PLUGINS_tag_dist($idb, &$boucles, $critere) {

	// Initialisation de la boucle concernée.
	$boucle = &$boucles[$idb];

	// Initialiser le hash de calcul de la condition qui ne sera rempli que si le critère est correct.
	$hash = '';

	if (!empty($critere->param)) {
		// On identifie le tag passé dans le critère.
		if (
			isset($critere->param[0])
			and ($parametre = $critere->param[0])
		) {
			$tag = calculer_liste($parametre, [], $boucles, $boucle->id_parent);
			$hash = '
			$where = $conditionner(\'tag\',' . $tag . ');';
		}
	} elseif (
		!empty($critere->cond)
		and ($critere->cond === '?')
	) {
		// Pas de tag explicite dans l'appel du critere mais un point d'interrogation.
		// -> on regarde si le tag est dans le contexte
		$hash = '
		$tag = interdire_scripts(entites_html(table_valeur($Pile[0]??[], (string)\'tag\', null), true));
		$where = $conditionner(\'tag\',$tag);
		';
	}

	if ($hash) {
		$boucle->hash .=  '
			// TAG PLUGIN
			$conditionner = \'calculer_critere_typologie_plugin\';';
		$boucle->hash .= $hash;
		$boucle->where[] = '$where';
	}
}

function calculer_critere_typologie_plugin($typologie, $type) {

	// Initialisation de la condition à vrai dans le cas où il faudrait ignorer le critère
	$condition = '1=1';

	if (
		(
			($typologie === 'categorie')
			or ($typologie === 'tag')
		)
		and $type
	) {
		include_spip('inc/svptype_plugin');
		if (
			(
				($typologie === 'categorie')
				and (
					($type === 'aucune')
					or ($type === 'toute')
				)
			)
			or (
				($typologie === 'tag')
				and (
					($type === 'aucun')
					or ($type === 'tout')
				)
			)
		) {
			$id_mot = 0;
			$condition = plugin_elaborer_condition($typologie, $id_mot);
			if (
				($type === 'toute')
				or ($type === 'tout')
			) {
				// On change le IN par un NOT IN
				$condition = str_replace(' IN ', ' NOT IN ', $condition);
			}
		} else {
			include_spip('inc/svptype_type_plugin');
			if ($id_mot = type_plugin_lire($typologie, $type, 'id_mot')) {
				$condition = plugin_elaborer_condition($typologie, intval($id_mot));
			}
		}
	}

	return $condition;
}

function pluginspip_affiche_droite($flux) {

	// Identification de la page et de l'objet
	$exec = $flux['args']['exec'];

	// Récupérer la liste des objets qui supporte une couleur
	include_spip('inc/config');
	$objets_config = lire_config('couleur_objet/objets', array());

	include_spip('inc/svptype_type_plugin');
	if (
		in_array('spip_mots', $objets_config) // si configuration objets ok
		and ($exec == 'type_plugin') // page d'un objet éditorial
		and ($id_objet = intval($flux['args']['id_mot']))
		and ($typologie = _request('typologie'))
		and ($typologie == 'categorie')
		and (type_plugin_lire($typologie, $id_objet, 'profondeur') == 0)
	) {
		$couleur = sql_getfetsel(
			'couleur_objet',
			'spip_couleur_objet_liens',
			array(
				'objet=' . sql_quote('mot'),
				'id_objet=' . $id_objet
			)
		);
		$contexte = array(
			'objet'         => 'mot',
			'id_objet'      => $id_objet,
			'couleur_objet' => $couleur
		);
		$flux['data'] .= recuperer_fond('inclure/couleur_objet', $contexte);
	}

	return $flux;
}

// Dépôts
// ------

/**
 * Retourne un texte indiquant un nombre total de contributions pour un dépot
 *
 * Calcule différents totaux pour un dépot donné et retourne un texte
 * de ces différents totaux. Les totaux correspondent par défaut aux
 * plugins et paquets, mais l'on peut demander le total des autres contributions
 * avec le second paramètre.
 *
 * @uses pluginspip_compter()
 * @param int $id_depot
 *     Identifiant du dépot
 *     Zéro (par défaut) signifie ici : «dans tous les dépots distants»
 *     (id_dépot>0) et non «dans le dépot local»
 * @param string $contrib
 *     Type de total demandé ('plugin' ou autre)
 *     Si 'plugin' : indique le nombre de plugins et de paquets du dépot
 *     Si autre chose : indique le nombre des autres contributions, c'est
 *     à dire des zips qui ne sont pas des plugins, comme certaines libraires ou
 *     certains jeux de squelettes.
 * @return string
 *     Texte indiquant certains totaux tel que nombre de plugins, nombre de paquets...
 **/
function depot_compter_contributions($id_depot, $contrib = 'plugin') {

	$total = pluginspip_compter('depot', $id_depot);
	if (!$id_depot) {
		$info = _T('svp:info_depots_disponibles', array('total_depots' => $total['depot'])) . ', ' .
			_T('svp:info_plugins_heberges', array('total_plugins' => $total['plugin'])) . ', ' .
			_T('svp:info_paquets_disponibles', array('total_paquets' => $total['paquet']));
	} elseif ($contrib == 'plugin') {
			$info = _T('svp:info_plugins_heberges', array('total_plugins' => $total['plugin'])) . ', ' .
				_T('svp:info_paquets_disponibles', array('total_paquets' => $total['paquet'] - $total['autre']));
	} else {
			$info = _T('svp:info_contributions_hebergees', array('total_autres' => $total['autre']));
	}

	return $info;
}

// Plugins & paquets
// -----------------

function plugin_calculer_rss($url_doc) {

	// Initialisation du flux rss
	$rss = '';

	// Si l'url de la documentation existe et fait référence à SPIP Contrib on calcul le flux RSS à afficher.
	if ($url_doc
	and ((stripos($url_doc, 'contrib.spip.net') !== false) or (stripos($url_doc, 'www.spip-contrib.net') !== false))) {
		// On extrait l'article de l'url afin de vérifier si c'est un id. Si c'est le cas on le modifie en article<id>.
		$article = basename($url_doc);
		if ($article and (stripos($url_doc, 'rubrique') === false)) {
			if (intval($article)) {
				$url_doc = str_replace("/${article}", "/article${article}", $url_doc);
			}

			// On peut maintenant calculer le flux RSS.
			$rss = copie_locale("https://contrib.spip.net/spip.php?page=backend-forum-url&url=${url_doc}", 'modif');
		}
	}

	return $rss;
}

function plugin_informer_autodoc($source) {

	// Initialisation de l'url autodoc.
	$url_autodoc = '';

	// On stocke en static le cache de la liste des plugins fournissant un PHPDoc
	static $autodoc = null;


	if ($autodoc == null) {
		// Le tableau n'a pas encore été initialisé, il faut charger le cache.
		// -- Initialisation de l'identifiant du cache autodoc
		$dir_cache = sous_repertoire(_DIR_VAR, 'cache-autodoc');
		$cache = $dir_cache . 'autodoc.json';

		// On vérifie si on doit recalculer le cache ou pas.
		include_spip('inc/flock');
		if (!file_exists($cache)
		or (!filemtime($cache) or (time() - filemtime($cache) > _PLUGINSPIP_TIMEOUT_AUTODOC))) {
			// On recharge le fichier JSON fourni par le site idoine.
			// Acquisition des données spécifiées par l'url
			include_spip('inc/distant');
			$url = 'https://code.spip.net/index_autodoc.json';
			$flux = recuperer_url($url, array('transcoder' => true));

			$plugins = array();
			$erreur = '';
			if (empty($flux['page'])) {
				spip_log("URL indiponible : ${url}", _LOG_ERREUR);
			} else {
				// Transformation de la chaîne json reçue en tableau associatif
				try {
					$page = json_decode($flux['page'], true);
					if (!empty($page['repositories'])) {
						$plugins = $page['repositories'];
					} else {
						spip_log("Erreur fichier vide ou introuvable à l'URL `${url}` : " . $erreur->getMessage(), _LOG_ERREUR);
					}
				} catch (Exception $erreur) {
					spip_log("Erreur d'analyse JSON pour l'URL `${url}` : " . $erreur->getMessage(), _LOG_ERREUR);
				}
			}

			// In fine, on met à jour le cache
			ecrire_fichier($cache, json_encode($plugins));
		}

		// On lit le contenu JSON du cache et on le décode dans le tableau statique.
		lire_fichier($cache, $autodoc);
		$autodoc = json_decode($autodoc, true);
	}

	// On détermine si le source du paquet correspond bien à un autodoc et si oui on renvoie l'url
	if (!empty($autodoc[$source])) {
		$url_autodoc = $autodoc[$source];
	}

	return $url_autodoc;
}


function plugin_informer_source($src_archive) {

	// Initialisation à vide de l'url des sources si le serveur n'est pas reconnu.
	$url_source = '';

	// Détermination du serveur des sources et construction de l'url des sources.
	if ($serveur = plugin_determiner_serveur($src_archive)) {
		switch ($serveur) {
			case 'zone':
				// Le zip du plugin provient de la zone, l'url est relative et il faut la compléter.
				$url_source = "https://zone.spip.org/trac/spip-zone/browser/${src_archive}";
				break;
			case 'github':
			case 'gitspip':
				// Le zip du plugin provient de la forge SPIP ou de Github, l'url fourni dans src_archive est
				// donc celle exacte du source.
				// C'est le cas des plugins sous gitea ou github et fabriqué par le débardeur.
				$url_source = $src_archive;
				break;
		}
	}

	return $url_source;
}


function plugin_informer_paquet($src_archive, $version, $raw = true) {

	// Initialisation à vide de l'url des sources si le serveur n'est pas reconnu.
	$url_paquet = '';

	// Détermination du serveur des sources et construction de l'url des sources.
	if ($serveur = plugin_determiner_serveur($src_archive)) {
		// Dénormaliser la version de façon à construire l'éventuel tag
		include_spip('svp_fonctions');
		$version = denormaliser_version($version);
		switch ($serveur) {
			case 'zone':
				// Le zip du plugin provient de la zone, l'url est relative et il faut la compléter.
				$url_paquet = plugin_informer_source($src_archive) . '/paquet.xml?format=txt';
				break;
			case 'github':
				// Le zip du plugin provient de Github.
				// -- on utilise le tag associé pour calculer l'url du paquet
				$url_paquet = substr($src_archive, 0, -4)
					. '/' . ($raw ? 'raw' : 'blob') . '/v'
					. $version
					. '/paquet.xml';
				break;
			case 'gitspip':
				// Le zip du plugin provient de la forge SPIP.
				// -- on utilise le tag associé pour calculer l'url du paquet
				$url_paquet = substr($src_archive, 0, -4)
					. '/' . ($raw ? 'raw' : 'tag') . '/v'
					. $version
					. '/paquet.xml';
				break;
		}

		if (
			$url_paquet
			and include_spip('inc/distant')
			and !recuperer_infos_distantes($url_paquet)) {
			$url_paquet = '';
		}
	}

	return $url_paquet;
}


function plugin_informer_commit($src_archive) {

	// Initialisation à vide de l'url de l'archive qui sera retourné si on arrive pas à calculer l'url complète.
	$flux = array('format' => '', 'url' => '');

	// Détermination du serveur des sources et construction de l'url des sources.
	if ($serveur = plugin_determiner_serveur($src_archive)) {
		switch ($serveur) {
			case 'zone':
				// Le zip du plugin provient de la zone
				// - l'url est relative et il faut la compléter.
				// - le flux des commits est au format RSS
				$flux['url'] = "https://zone.spip.org/trac/spip-zone/log/${src_archive}?format=rss&limit=10&mode=stop_on_copy";
				$flux['format'] = 'rss';
				break;
			case 'github':
				// Cas d'un plugin développé sur GitHub:
				// - le flux des commits est au format ATOM
				$flux['url'] = substr($src_archive, 0, -4) . '/commits.atom';
				$flux['format'] = 'atom';
				break;
			case 'gitspip':
				// Le zip du plugin provient de la forge SPIP, pas de flux, il faut passer par l'API REST.
				$flux['url'] = str_replace('https://git.spip.net', 'https://git.spip.net/api/v1/repos', substr($src_archive, 0, -4)) . '/commits?limit=10';
				$flux['format'] = 'gitea';
				break;
		}
	}

	return $flux;
}


function plugin_informer_issue($src_archive) {

	// Initialisation à vide de l'url de l'archive qui sera retourné si on arrive pas à calculer l'url complète.
	$flux = array('format' => '', 'url' => '');

	// Détermination du serveur des sources et construction de l'url des sources.
	if ($serveur = plugin_determiner_serveur($src_archive)) {
		switch ($serveur) {
			case 'zone':
				// Le zip du plugin provient de la zone
				// -> pas de gestion de tickets
				break;
			case 'github':
				// Cas d'un plugin développé sur GitHub:
				// -> TODO : a coder
				break;
			case 'gitspip':
				// Le zip du plugin provient de la forge SPIP, pas de flux, il faut passer par l'API REST.
				$flux['url'] = str_replace('https://git.spip.net', 'https://git.spip.net/api/v1/repos', substr($src_archive, 0, -4)) . '/issues?state=open&type=issues&limit=10';
				$flux['format'] = 'gitea';
				break;
		}
	}

	return $flux;
}

/**
 * Renvoie, pour un plugin donné, la catégorie affectée ou la chaine vide si aucune affectation n'a encore été faite.
 * Il est possible de retourner uniquement la catégorie parent.
 *
 * @param int|string $plugin La valeur du préfixe ou de l'id du plugin.
 * @param string     $niveau Indique si l'on veut la catégorie complète ou uniquement l'identifiant
 *                           de la racine ou de la feuille. Prend les valeurs `''` par défaut, `racine` ou `feuille`.
 *
 * @return string Identifiant de la catégorie ou de la catégorie racine uniquement.
 */
function plugin_informer_categorie($plugin, $niveau = '') {

	// On détermine le préfixe du plugin qui est utilisé dans la table des affectations :
	// -- si c'est le préfixe on le passe en majuscules pour être cohérent avec le stockage en base.
	// -- sinon on lit le préfixe à partir de l'id du plugin.
	if ($id_plugin = intval($plugin)) {
		include_spip('inc/svp_plugin');
		$prefixe = plugin_lire($id_plugin, 'prefixe');
	} else {
		$prefixe = strtoupper($plugin);
	}

	// On récupère la catégorie du plugin au format id_mot
	include_spip('inc/svptype_plugin');
	$affectation = plugin_lister_type_plugin($prefixe, 'categorie');
	if ($affectation) {
		// Il ne peut y avoir qu'une seul catégorie, connue par son id.
		// -- on retourne son identifiant
		include_spip('inc/svptype_mot');
		$categorie = mot_lire_identifiant($affectation[0]);
	} else {
		$categorie = 'aucune';
	}

	// On extrait la catégorie racine. Si elle existe, la catégorie d'un plugin est toujours
	// sous la forme xxx/yyy: on extrait xxx.
	if (
		$categorie !== 'aucune'
		and (strpos($categorie, '/') !== false)
		and $niveau
	) {
		$types_plugin = explode('/', $categorie);
		$categorie = $niveau == 'racine' ? $types_plugin[0] : $types_plugin[1];
	}

	return $categorie;
}

function plugin_determiner_serveur($src_archive) {

	// Dossiers de niveau 1 de la zone
	static $racines_zone = array(
		'_plugins_',
		'_squelettes_',
		'_themes_',
		'_grenier_',
		'_galaxie_',
		'tags_'
	);

	// Initialise le serveur à vide pour indiquer une erreur (serveur inconnu).
	$serveur = '';

	if (substr($src_archive, 0, 18) == 'https://github.com') {
		$serveur = 'github';
	} elseif (substr($src_archive, 0, 20) == 'https://git.spip.net') {
		$serveur = 'gitspip';
	} elseif (in_array(substr($src_archive, 0, strpos($src_archive, '/')), $racines_zone)) {
		$serveur = 'zone';
	}

	return $serveur;
}

/**
 * Retourne un texte indiquant un nombre total de plugins
 *
 * Calcule le nombre de plugins correspondant à certaines contraintes,
 * tel que l'appartenance à un certain dépot, une certaine catégorie
 * ou une certaine branche de SPIP et retourne une phrase traduite
 * tel que «64 plugins disponibles»
 *
 * @uses pluginspip_compter()
 * @param int $id_depot
 *     Identifiant du dépot
 *     Zéro (par défaut) signifie ici : «dans tous les dépots distants»
 *     (id_dépot>0) et non «dans le dépot local»
 * @param string $categorie
 *     Type de catégorie (auteur, communication, date...)
 * @param string $compatible_spip
 *     Numéro de branche de SPIP. (3.0, 2.1, ...)
 * @return string
 *     Texte indiquant un nombre total de paquets
 **/
function plugin_compter($id_depot = 0, $categorie = '', $compatible_spip = '', $sortie = 'message') {
	$decompte = pluginspip_compter('plugin', $id_depot, $categorie, $compatible_spip);
	$retour = ($sortie === 'message')
		? _T('svp:info_plugins_disponibles', array('total_plugins' => $decompte['plugin']))
		: $decompte['plugin'];

	return $retour;
}

/**
 * Teste si un plugin possède des dépendances
 *
 * @param string $balise_serialisee
 *     Informations des dépendances (tableau sérialisé) tel que stocké
 *     en base dans la table spip_paquets
 * @return bool
 *     Le plugin possède t'il des dépendances ?
 **/
function plugin_dependance_existe($balise_serialisee) {
	$dependances = unserialize($balise_serialisee);
	foreach ($dependances as $_dependance) {
		if ($_dependance) {
			return true;
		}
	}

	return false;
}

/**
 * Retourne un texte indiquant un nombre total de paquets
 *
 * Calcule le nombre de paquets correspondant à certaines contraintes,
 * tel que l'appartenance à un certain dépot, une certaine catégorie
 * ou une certaine branche de SPIP et retourne une phrase traduite
 * tel que «78 paquets disponibles»
 *
 * @uses pluginspip_compter()
 * @param int $id_depot
 *     Identifiant du dépot
 *     Zéro (par défaut) signifie ici : «dans tous les dépots distants»
 *     (id_dépot>0) et non «dans le dépot local»
 * @param string $categorie
 *     Type de catégorie (auteur, communication, date...)
 * @param string $compatible_spip
 *     Numéro de branche de SPIP. (3.0, 2.1, ...)
 * @return string
 *     Texte indiquant un nombre total de paquets
 **/
function paquet_compter_telechargements($id_depot = 0, $categorie = '', $compatible_spip = '') {
	$total = pluginspip_compter('paquet', $id_depot, $categorie, $compatible_spip);
	$info = _T('svp:info_paquets_disponibles', array('total_paquets' => $total['paquet']));

	return $info;
}

// Autres
// ------

/**
 * Compte le nombre de plugins, paquets ou autres contributions
 * en fonction de l'entité demandée et de contraintes
 *
 * Calcule, pour un type d'entité demandé (depot, plugin, paquet, catégorie)
 * leur nombre en fonction de certaines contraintes, tel que l'appartenance
 * à un certain dépot, une certaine catégorie ou une certaine branche de SPIP.
 *
 * Lorsque l'entité demandée est un dépot, le tableau des totaux possède,
 * en plus du nombre de dépots, le nombre de plugins et paquets.
 *
 * @note
 *     Attention le critère de compatibilite SPIP pris en compte est uniquement
 *     celui d'une branche SPIP
 *
 * @param string $entite
 *     De quoi veut-on obtenir des comptes. Peut être 'depot', 'plugin',
 *    'paquet' ou 'categorie'
 * @param int $id_depot
 *     Identifiant du dépot
 *     Zéro (par défaut) signifie ici : «dans tous les dépots distants»
 *     (id_dépot>0) et non «dans le dépot local»
 * @param string $categorie
 *     Type de catégorie de niveau racine (auteur, communication, date...)
 * @param string $compatible_spip
 *     Numéro de branche de SPIP. (3.0, 2.1, ...)
 * @return array
 *     Couples (entite => nombre).
 **/
function pluginspip_compter($entite, $id_depot = 0, $categorie = '', $compatible_spip = '') {
	$compteurs = array();

	$where = array();
	if ($id_depot) {
		$where[] = "t1.id_depot=" . sql_quote($id_depot);
	} else {
		$where[] = "t1.id_depot>0";
	}

	// Récupérer la liste des plugins ayant la catégorie demandée, si la catégorie a été passée en argument
	// -- la catégorie est toujours une racine
	if ($categorie) {
		include_spip('inc/svptype_plugin');
		if ($categorie === 'aucune') {
			$condition_categorie = plugin_elaborer_condition('categorie', 0);
		} elseif ($categorie) {
			include_spip('action/editer_objet');
			$options = array(
				'champ_id' => 'identifiant',
				'champs' => 'id_mot'
			);
			$id_mot = objet_lire('mot', $categorie, $options);
			$condition_categorie = plugin_elaborer_condition('categorie', $id_mot);
		}
	}

	if ($entite === 'plugin') {
		$from = array('spip_plugins AS plugins', 'spip_depots_plugins AS t1');
		$where[] = "t1.id_plugin=plugins.id_plugin";
		if ($categorie) {
			$where[] = $condition_categorie;
		}
		if ($compatible_spip) {
			$creer_where = charger_fonction('where_compatible_spip', 'inc');
			$where[] = $creer_where($compatible_spip, 'plugins', '>');
		}
		$compteurs['plugin'] = sql_count(sql_select('DISTINCT t1.id_plugin', $from, $where));
	} elseif ($entite === 'paquet') {
		$from = array('spip_paquets AS t1');
		if ($categorie) {
			$plugins = sql_allfetsel('prefixe', 'spip_plugins as plugins', $condition_categorie);
			$plugins = array_column($plugins, 'prefixe');
			$where[] = sql_in('t1.prefixe', $plugins);
		}
		if ($compatible_spip) {
			$creer_where = charger_fonction('where_compatible_spip', 'inc');
			$where[] = $creer_where($compatible_spip, 't1', '>');
		}
		$compteurs['paquet'] = sql_countsel($from, $where);
	} elseif ($entite === 'depot') {
		$from = array('spip_depots AS t1');
		$select = array(
			'COUNT(t1.id_depot) AS depot',
			'SUM(t1.nbr_plugins) AS plugin',
			'SUM(t1.nbr_paquets) AS paquet',
			'SUM(t1.nbr_autres) AS autre'
		);
		$compteurs = sql_fetsel($select, $from, $where);
	} elseif ($entite === 'categorie') {
		$from = array('spip_plugins AS t2', 'spip_depots_plugins AS t1');
		$where[] = "t1.id_plugin=t2.id_plugin";
		if ($id_depot) {
			$ids = sql_allfetsel('id_plugin', 'spip_depots_plugins AS t1', $where);
			$ids = array_column($ids, 'id_plugin');
			$where[] = sql_in('t2.id_plugin', $ids);
		}
		if ($compatible_spip) {
			$creer_where = charger_fonction('where_compatible_spip', 'inc');
			$where[] = $creer_where($compatible_spip, 't2', '>');
		}
		if ($categorie) {
			$plugins = sql_allfetsel('prefixe', 'spip_plugins as plugins', $condition_categorie);
			$plugins = array_column($plugins, 'prefixe');
			$where[] = sql_in("t2.prefixe", $plugins);
			$compteurs['categorie'] = sql_count(sql_select('DISTINCT t1.id_plugin', $from, $where));
		} else {
			foreach (categorie_lister('id') as $_categorie) {
				if ($_categorie !== 'aucune') {
					$affectations = type_plugin_repertorier_affectation('categorie', array('type_racine' => $_categorie));
					if ($affectations) {
						$plugins = array_column($affectations, 'prefixe');
						$where[] = sql_in("t2.prefixe", $plugins);
						$compteurs['categorie'][$_categorie] = sql_count(sql_select('DISTINCT t1.id_plugin', $from, $where));
					} else {
						$compteurs['categorie'][$_categorie] = 0;
					}
				}
			}
		}
	}

	return $compteurs;
}

/**
 * Compile la balise `#BRANCHES_SPIP_LISTE`
 *
 * Cette balise retourne une liste des branches de SPIP
 *
 * Avec un paramètre indiquant une branche, la balise retourne
 * une liste des bornes mini et maxi de cette branche.
 *
 * @example
 *     #BRANCHES_SPIP_LISTE       : array('1.9', '2.0', '2.1', ....)
 *     #BRANCHES_SPIP_LISTE{3.0}  : array('3.0.0', '3.0.999')
 *
 * @balise
 * @see branches_spip_lister()
 *
 * @param Champ $p
 *     Pile au niveau de la balise
 * @return Champ
 *     Pile complétée par le code à générer
 **/
function balise_BRANCHES_SPIP_LISTE($p) {
	// nom d'une branche en premier argument
	if (!$branche = interprete_argument_balise(1, $p)) {
		$branche = "''";
	}
	$p->code = 'branches_spip_lister(' . $branche . ')';

	return $p;
}

/**
 * Retourne une liste des branches de SPIP, ou les bornes mini et maxi
 * d'une branche donnée
 *
 * @param string $branche
 *     Branche dont on veut récupérer les bornes mini et maxi
 * @return array
 *     Liste des branches array('1.9', '2.0', '2.1', ....)
 *     ou liste mini et maxi d'une branche array('3.0.0', '3.0.999')
 **/
function branches_spip_lister($branche) {

	$retour = array();

	// pour $GLOBALS['infos_branches_spip']
	include_spip('inc/svp_outiller');
	$svp_branches = $GLOBALS['infos_branches_spip'];

	if (is_array($svp_branches)) {
		if (($branche) and in_array($branche, $svp_branches)) {
			// On renvoie les bornes inf et sup de la branche specifiee
			$retour = $svp_branches[$branche];
		} else {
			// On renvoie uniquement les numeros de branches
			$retour = array_keys($svp_branches);
		}
	}

	return $retour;
}
