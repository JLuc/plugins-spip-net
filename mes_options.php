<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

// prefs svp
define('_SVP_CRON_ACTUALISATION_STATS', true);
define('_SVP_MODE_RUNTIME', false);

// utile en prod ?
define('_LOG_FILTRE_GRAVITE',8);

// utile ?
define('_AUTOBR', '');

define('_DIR_PLUGINS_SUPPL', _DIR_RACINE . 'squelettes/plugins/');
$GLOBALS['dossier_squelettes'] = 'squelettes/theme';

