<?php
    # fabrique le fichier d'aide en ligne demande
    # voir aussi inc-urls-trad.php3

#    header('Content-Type: text/html; charset=utf-8');

#    include('ecrire/inc_version.php');
#    $xhtml=false;	# sinon ca casse tout :))

#    define('aide_en_ligne', 'nous y sommes');
#    $fond = 'aide';

    ## on veut purger les notes a chaque section
    function propre_facon_aide($texte) {
      foreach (explode('<h2>', $texte) as $morceau) {
        $t = propre('<h2>'.trim($morceau));
        if ($n = $GLOBALS['les_notes']) {
          $GLOBALS['les_notes'] = '';
          $t .= "\n\n<div class='notes'><small>$n</small></div>\n";
        }
        $array[] = $t;
      }
      
      $texte = preg_replace(',^.*?<h2>,','',
        join ('', $array));

      #$texte = str_replace('var/cache-TeX/', 'https://www.spip.net/var/cache-TeX/', $texte);
      #$texte = str_replace('local/cache-TeX/', 'https://www.spip.net/local/cache-TeX/', $texte);
	# reperer et recopier les images temporaires
	$max = 100;
	while(preg_match(',local/(cache[^/]*)/([^.]*.(gif|png|jpg)),',$texte,$regs) && $max--){
		@copy($regs[0], $f=('AIDE/local-'.$regs[1].'-'.$regs[2]));
		$texte = str_replace($regs[0],'https://plugins.spip.net/'.$f,$texte);
	}
	# reperer et rediriger les images de IMG
	/*while(preg_match(',AIDE/([^/]*)/([^.]*.(gif|png|jpg)),',$texte,$regs) && $max--){
		if (file_exists($f = _DIR_IMG.$regs[2])
		  OR file_exists($f = _DIR_IMG.$regs[3].'/'.$regs[2]))
		$texte = str_replace($regs[0],'https://www.spip.net/'.$f,$texte);
	}*/

      return $texte;
    }

    ## transitoire : aller chercher l'aide dans le fichier aide_tmp/oc-aide.html
    function aide_tmp($rien) {
      global $lang_aide;
        if (file_exists($f = "aide_tmp/$lang_aide-aide.html")) {
                return join ('', file($f));
        }    
        if ($lang_aide == 'pt_br')
          return file_get_contents("aide_tmp/pt-aide.html");
    }


#    include('spip.php');
